FROM openjdk:17-jdk-alpine
COPY target/jenkins-0.0.1-SNAPSHOT.jar demo.jar
ENTRYPOINT ["java", "-jar", "demo.jar"]
